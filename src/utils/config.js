import firebase from 'firebase'

const config = {
  apiKey: "AIzaSyA4FuhW11rlyHcxzWFflhsSyUjs-pL_QYQ",
  authDomain: "gobony-cef73.firebaseapp.com",
  databaseURL: "https://gobony-cef73.firebaseio.com",
  projectId: "gobony-cef73",
  storageBucket: "gobony-cef73.appspot.com",
  messagingSenderId: "857514807997",
  appId: "1:857514807997:web:60cc75fa6191a2ce271dd4",
  measurementId: "G-83JP4YN32H"
}

firebase.initializeApp(config);

export default firebase