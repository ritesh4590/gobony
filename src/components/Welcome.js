import React, { Component } from 'react'
import { Redirect } from 'react-router'
import firebase from 'firebase'
import Button from '@material-ui/core/Button';
import '../App.css'
class Welcome extends Component {

  constructor(props) {
    super(props)

    this.state = {
      redirect: false
    }
  }

  userLogout = () => {
    firebase.auth().signOut()
      .then(res => {
        this.setState({
          redirect: true
        })
      })
      .catch(err => {
        console.log(err)
      })
  }
  render() {
    const { redirect } = this.state
    if (redirect) {
      return (<Redirect to="/" />)
    }
    else {
      return (
        <div className="welcome">
          <h2>Welcome to Gobony Dashboard</h2>
          <div className="welcome-btn-class">
            <Button onClick={this.userLogout} variant="contained" color="primary">
              Logout
            </Button>
          </div>
        </div>
      )
    }
  }
}

export default Welcome
