import React, { Component } from 'react'
import '../App.css'
import firebase from 'firebase'
import { Redirect } from 'react-router-dom'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';

class SignUp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      errorText: '',
      redirect: false
    }
  }

  onFormSubmit = (e) => {
    e.preventDefault()

    firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(res => {
        this.setState({
          redirect: true
        })
      })
      .catch(err => {
        this.setState({
          errorText: err.message
        })
      })
  }

  onEmailChange = (e) => {
    this.setState({
      email: e.target.value
    })

  }
  onPasswordChange = (e) => {
    this.setState({
      password: e.target.value
    })

  }
  render() {
    const { email, password, redirect, errorText } = this.state

    if (redirect) {
      return (<Redirect to="/" />)
    }
    else {
      return (
        <div className="signup">
          <h1>Gobony Sign up</h1>
          <br />

          {
            errorText ? <Alert severity="error">{errorText}</Alert> : null
          }
          <br />

          <form onSubmit={this.onFormSubmit}>
            <div>
              <TextField id="outlined-basic" label="Email" variant="outlined" value={email} onChange={this.onEmailChange} />
            </div>
            <br />
            <div>
              <TextField type="password" id="outlined-basic" label="Password" variant="outlined" value={password} onChange={this.onPasswordChange} />
            </div>
            <br />
            <div className="btn-class">
              <Button onClick={this.onFormSubmit} variant="contained" color="primary">
                Sign up
            </Button>
            </div>
          </form>
          <h4><a href="/">Login</a></h4>
        </div>
      )
    }
  }
}

export default SignUp