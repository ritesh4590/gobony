import React, { Component } from 'react'
import '../App.css'
import { Redirect } from 'react-router-dom'
import firebase from '../utils/config'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';



class Login extends Component {

  constructor(props) {

    super(props)
    this.state = {
      email: '',
      password: '',
      errorText: '',
      redirect: false
    }
  }

  onFormSubmit = (e) => {
    e.preventDefault()
    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(res => {
        this.setState({
          redirect: true
        })

      })
      .catch(err => {
        console.log("Login-error", err.message)
        this.setState({
          errorText: err.message
        })
      })
  }

  onEmailChange = (e) => {
    this.setState({
      email: e.target.value
    })

  }
  onPasswordChange = (e) => {
    this.setState({
      password: e.target.value
    })

  }
  render() {
    const { email, password, redirect, errorText } = this.state

    if (redirect) {
      return (<Redirect to="/welcome" />)
    }
    else {
      return (
        <div className="login">
          <h1>Gobony Login</h1>
          <br />
          {
            errorText ? <Alert severity="error">{errorText}</Alert> : null
          }
          <br />
          <form onSubmit={this.onFormSubmit} id="login-form">
            <div>
              <TextField id="outlined-basic" className="material-input" label="Email" variant="outlined" value={email} onChange={this.onEmailChange} />
            </div>
            <br />
            <div>
              <TextField type="password" label="Password" variant="outlined" value={password} onChange={this.onPasswordChange} />
            </div>
            <br />
            <div className="btn-class">
              <Button onClick={this.onFormSubmit} variant="contained" color="primary">
                Login
            </Button>
            </div>
          </form>
          <h4>Don't have an account??
        <a href="/signup">
              Sign Up
        </a>
          </h4>
        </div>
      )
    }
  }
}

export default Login
