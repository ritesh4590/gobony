import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom'
import Login from './components/Login'
import SignUp from './components/SignUp'
import Welcome from './components/Welcome'
import firebase from './utils/config'
import Container from '@material-ui/core/Container';



class App extends Component {
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      user ? console.log("User in Logged in-->>", user) : console.log("User in Logged out-->>", user)
    })
  }

  render() {
    return (
      <Container>
        <div className="App" >
          <BrowserRouter>
            <div>
              <Route path="/" component={Login} exact />
              <Route path="/signup" component={SignUp} exact />
              <Route path="/welcome" component={Welcome} exact />
            </div>
          </BrowserRouter>
        </div>
      </Container>
    );
  }
}
export default App;
